**Day 1:  Intoduction to version control Git/Gitlab**
- Create a gitlab account using your work email
- Practice the git command using this interactive tools https://www.katacoda.com/courses/git
- Complete this git assignment https://gitlab.com/symb-assessment/git-assignment

**Day 2: Intoduction to HTML/CSS/Bootstrap**
- Learn HTML from https://www.w3schools.com/html/
- Learn CSS from https://www.w3schools.com/css/
- Learn bootstrap from https://www.w3schools.com/bootstrap4/
- Create your own portfolio using HTML/CSS/Bootstrap
- Create a repository on gitlab and push the code for above created portfolio on this repository

**Day 3: Intoduction to JS/Jquery**
- Learn JS from https://www.w3schools.com/js/DEFAULT.asp 
- Learn jquery from https://www.w3schools.com/jquery/
- Create this static website https://xd.adobe.com/view/0366b032-b6f8-423a-738e-09d445f7279d-7557/
- Create a repository on gitlab and push the code for above created website on this repository

**Day 4: Intoduction to Django**
- Refer the below tutorial/doc to learn Django and its funtionality
- https://www.djangoproject.com/start/
- https://www.youtube.com/watch?v=UmljXZIypDc&list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p

**Day 5: Your fist Django Application**
- Creae polls application with the help of this Django documenttation https://docs.djangoproject.com/en/3.1/intro/tutorial01/
- Create a repository on gitlab and push the code for above created Django application on this repository

**Day 6: Your second Django Application**
- Create a Django application and connect to MYSQL DB
- Create user registration, user login and user logout
- Create the user login with google and facebook
- You can follow this document for social authentication https://realpython.com/adding-social-authentication-to-django/
- Please create the seperate repository on gitlab for this project and follow the proper step for commiting and pushing code

**Day 7: Continuation on second Django Application**
- Create forgot password and change password funtionality
- Forgot password will be visiable on login page and when user click on forgot password he will get and email with the link to change password
- You can use SMTP for sending email https://docs.djangoproject.com/en/3.1/topics/email/

**Day 8: Continuation on second Django Application**
- Now create the template for this design https://xd.adobe.com/view/0366b032-b6f8-423a-738e-09d445f7279d-7557/ and serve this page only when user is login
- Content for this page will be dynamically loaded from the backend
- You can user Django admin to fill the details like country and their details

**Day 9: Your first CRUD API**
- Integrate the sentry in the above application to handle unhandled exception https://docs.sentry.io/platforms/python/guides/django/
- Learn Django rest framework and create the api for the below funtionality in the above created Django Application
1. List country
2. Create country
3. Update country,
4. Delete country
5. Get country details for given country by their id
6. Create country details for given country by their id
7. Update and delete country for given country by their id

**Day 10: Unit testing in Django**
- Learn what is unit testing and how to write unit testing in Django application with the help of https://docs.djangoproject.com/en/3.1/topics/testing/overview/
- Write unit test cases for all the CRUD operation in the above created Django application

**Day 11: Payment Gateway**
- Read what is Payment Gateway and how to use it
- What are diffrerent ways to accept Payment
- Finalize which Payment Gateway service proider to use
- Read all the api of this proider

**Day 12: Payment Gateway Integration**
- Integrate the above Payment Gateway in your application to accept the Payment

**Day 13: Admin changes**
- In the above created django application in the UI we have logo for country
- Please implement in way so that admin can change the country logo from the admin backend and it should reflect on the web pages
- Create a service in a way so that we can authenticate user for a specified time(this can be updated from backend)

**Day 14: Email sending(Send Grid)**
- Create Send Grid account and read their API and integrate with application for ending email with attachment
- If the user is login and their specified time period is expired then send an email with pre approved template to the user



